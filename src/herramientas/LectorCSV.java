/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;
import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author czarate
 */
public class LectorCSV {
    
    private CSVReader reader;
    
    public LectorCSV(String url){
        try{
            reader=new CSVReader(new FileReader(url));
        }catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public Object[] leerColumna(int columna){
        List<Object> valores=new ArrayList();
        try{
            for(String[] v:reader.readAll()){
                valores.add(new Double(v[columna]));
            }
        }catch(IOException ex){ 
            System.out.println(ex.getMessage());
        }
        return valores.toArray();     
    }
    
    public void cerrar(){
        try {
            reader.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
