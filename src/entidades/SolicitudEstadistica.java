/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author czarate
 */
public class SolicitudEstadistica {
    
    private String nombre;
    private double rango_minimo;
    private double rango_maximo;
    private double media;
    private double mediana;
    private double moda;
    private double varianza;
    private double desviacion_estandar;
    private double coeficiente_variacion;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the rango_minimo
     */
    public double getRango_minimo() {
        return rango_minimo;
    }

    /**
     * @param rango_minimo the rango_minimo to set
     */
    public void setRango_minimo(double rango_minimo) {
        this.rango_minimo = rango_minimo;
    }

    /**
     * @return the rango_maximo
     */
    public double getRango_maximo() {
        return rango_maximo;
    }

    /**
     * @param rango_maximo the rango_maximo to set
     */
    public void setRango_maximo(double rango_maximo) {
        this.rango_maximo = rango_maximo;
    }

    /**
     * @return the media
     */
    public double getMedia() {
        return media;
    }

    /**
     * @param media the media to set
     */
    public void setMedia(double media) {
        this.media = media;
    }

    /**
     * @return the mediana
     */
    public double getMediana() {
        return mediana;
    }

    /**
     * @param mediana the mediana to set
     */
    public void setMediana(double mediana) {
        this.mediana = mediana;
    }

    /**
     * @return the moda
     */
    public double getModa() {
        return moda;
    }

    /**
     * @param moda the moda to set
     */
    public void setModa(double moda) {
        this.moda = moda;
    }

    /**
     * @return the varianza
     */
    public double getVarianza() {
        return varianza;
    }

    /**
     * @param varianza the varianza to set
     */
    public void setVarianza(double varianza) {
        this.varianza = varianza;
    }

    /**
     * @return the desviacion_estandar
     */
    public double getDesviacion_estandar() {
        return desviacion_estandar;
    }

    /**
     * @param desviacion_estandar the desviacion_estandar to set
     */
    public void setDesviacion_estandar(double desviacion_estandar) {
        this.desviacion_estandar = desviacion_estandar;
    }

    /**
     * @return the coeficiente_variacion
     */
    public double getCoeficiente_variacion() {
        return coeficiente_variacion;
    }

    /**
     * @param coeficiente_variacion the coeficiente_variacion to set
     */
    public void setCoeficiente_variacion(double coeficiente_variacion) {
        this.coeficiente_variacion = coeficiente_variacion;
    }
    
    /**
     * @return array objects
     */
    public Object[] toArrayObjects() {
        Object[] arreglo=new Object[9];
        arreglo[0]=nombre;
        arreglo[1]=rango_minimo;
        arreglo[2]=rango_maximo;
        arreglo[3]=media;
        arreglo[4]=mediana;
        arreglo[5]=moda;
        arreglo[6]=varianza;
        arreglo[7]=desviacion_estandar;
        arreglo[8]=coeficiente_variacion;
        return arreglo;
    }
    
}
