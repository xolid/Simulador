/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author czarate
 */
public class Solicitud {
    
    private double duracion;
    private int categoria;

    /**
     * @return the duracion
     */
    public double getTiempo() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setTiempo(double tiempo) {
        this.duracion = tiempo;
    }

    /**
     * @return the categoria
     */
    public int getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }
    
}
