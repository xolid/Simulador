/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author czarate
 */
public class EntradaEstadistica {
    
    private double media;
    private double mediana;
    private double moda;
    private double varianza;
    private double desviacion_estandar;
    private double coeficiente_variacion;

    /**
     * @return the media
     */
    public double getMedia() {
        return media;
    }

    /**
     * @param media the media to set
     */
    public void setMedia(double media) {
        this.media = media;
    }

    /**
     * @return the mediana
     */
    public double getMediana() {
        return mediana;
    }

    /**
     * @param mediana the mediana to set
     */
    public void setMediana(double mediana) {
        this.mediana = mediana;
    }

    /**
     * @return the moda
     */
    public double getModa() {
        return moda;
    }

    /**
     * @param moda the moda to set
     */
    public void setModa(double moda) {
        this.moda = moda;
    }

    /**
     * @return the varianza
     */
    public double getVarianza() {
        return varianza;
    }

    /**
     * @param varianza the varianza to set
     */
    public void setVarianza(double varianza) {
        this.varianza = varianza;
    }

    /**
     * @return the desviacion_estandar
     */
    public double getDesviacion_estandar() {
        return desviacion_estandar;
    }

    /**
     * @param desviacion_estandar the desviacion_estandar to set
     */
    public void setDesviacion_estandar(double desviacion_estandar) {
        this.desviacion_estandar = desviacion_estandar;
    }

    /**
     * @return the coeficiente_variacion
     */
    public double getCoeficiente_variacion() {
        return coeficiente_variacion;
    }

    /**
     * @param coeficiente_variacion the coeficiente_variacion to set
     */
    public void setCoeficiente_variacion(double coeficiente_variacion) {
        this.coeficiente_variacion = coeficiente_variacion;
    }
    
    /**
     * @return array objects
     */
    public Object[] toArrayObjects() {
        Object[] arreglo=new Object[6];
        arreglo[0]=media;
        arreglo[1]=mediana;
        arreglo[2]=moda;
        arreglo[3]=varianza;
        arreglo[4]=desviacion_estandar;
        arreglo[5]=coeficiente_variacion;
        return arreglo;
    }
    
}
