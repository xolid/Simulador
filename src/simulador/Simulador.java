/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulador;

import generadores.Generador;
import generadores.GeneradorIngreso;
import generadores.GeneradorSolicitud;
import entidades.Solicitud;
import analizadores.DispersionCentral;
import herramientas.LectorCSV;
import analizadores.TendenciaCentral;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import presentacion.FrmPrincipal;

/**
 *
 * @author czarate
 */
public class Simulador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        //generador usado
//        Generador generador=new Generador(32,3,2,5);
//        //generando de ingresos
//        GeneradorIngreso gingreso=new GeneradorIngreso(12.5426189389854,11.9303556920942,generador);
//        System.out.print("Tiempos de Ingreso Generados: ");
//        for(int i=0;i<50;i++){
//            System.out.print(gingreso.generarTiempoIngreso()+"-");
//        }
//        System.out.print("\n");
//        //generador de categorias
//        GeneradorSolicitud gsolicitud=new GeneradorSolicitud(generador);
//        System.out.println("Solicitudes Generadas: ");
//        for(int i=0;i<50;i++){
//            Solicitud solicitud=gsolicitud.generarSolicitud();
//            System.out.print(solicitud.getTiempo()+":"+solicitud.getCategoria()+"-");
//        }
//        System.out.println("");
//        //lectura de archivos CSV
//        String url="c:\\ins.csv";
//        LectorCSV lector=new LectorCSV(url);
//        String[] linea;
//        int index=0;
//        while ((linea = lector.leer()) != null) {
//            System.out.print("Linea "+(index++)+":");
//            for(int i=0;i<linea.length;i++){
//                System.out.print(linea[i]+",");
//            }
//            System.out.println("");
//        }
        
//        String url="c:\\ins.csv";
//        LectorCSV lector=new LectorCSV(url);
//        Object[] valores=lector.leerColumna(0);
//        
//        TendenciaCentral tc=new TendenciaCentral(valores);
//        System.out.println("media: "+tc.obtenerMedia());
//        System.out.println("mediana: "+tc.obtenerMediana());
//        System.out.println("moda: "+tc.obtenerModa());
//        
//        DispersionCentral dc=new DispersionCentral(valores,tc.obtenerMedia());
//        System.out.println("varianza: "+dc.obtenerVarianza());
//        System.out.println("desviacion estandar: "+dc.obtenerDesviacionEstandar());
//        System.out.println("coeficiente de variacion: "+dc.obtenerCoeficienteVariacion());
        
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(UnsupportedLookAndFeelException|ClassNotFoundException|InstantiationException|IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
        FrmPrincipal principal=new FrmPrincipal();
        principal.setVisible(true);
        
    }
}
