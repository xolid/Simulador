/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generadores;

/**
 *
 * @author czarate
 */
public class Generador {

    private double x;
    private double a;
    private double c;
    private double m;
    
    public Generador(double m, double a, double x, double c) {
        this.m = m;
        this.a = a;
        this.x = x;
        this.c = c;
    }

    public double generar() {
        double r = ((a*x)+c)%m;
        x = r;
        r = r/m;
        return r;
    }

}