/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generadores;

import entidades.Solicitud;

/**
 *
 * @author czarate
 */
public class GeneradorSolicitud{
    
    private Generador generador;
    
    public GeneradorSolicitud(Generador generador){
        this.generador=generador;
    }
    
    public Solicitud generarSolicitud(){
        Solicitud solicitud=new Solicitud();
        int categoria=generarCategoria();
        solicitud.setCategoria(categoria);
        switch(categoria){
            case 0:
                solicitud.setTiempo(obtenerPoisson(3.98));
                break;
            case 1:
                solicitud.setTiempo(obtenerPoisson(4.40816326530612));
                break;
            case 2:
                solicitud.setTiempo(obtenerPoisson(3.73469387755102));
                break;
        }
        return solicitud;
    }
    
    public double obtenerPoisson(double lambda){
        double r=generador.generar();
        double sum=0;
        for(int x=0;sum<r;x++){
            double poisson=(Math.exp(-lambda)*Math.pow(lambda,x))/factorial(x);
            sum+=poisson;
            if(sum>=r){
                return x;
            }
        }
        return 0;
    }
    
    public int factorial(int numero){
        if(numero==0){
            return 1;
        }else{
            return numero*factorial(numero-1);
        }
    }
    
    public int generarCategoria(){
        double valor=generador.generar();
        if(valor>=0&&valor<=0.329704058566125){
            return 0;
        }
        if(valor>=0.329704058566126&&valor<=0.663991372695411){
            return 1;
        }
        if(valor>=0.663991372695412&&valor<=1){
            return 2;
        }
        return -1;
    }
    
}
