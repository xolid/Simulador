/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package generadores;

/**
 *
 * @author czarate
 */
public class GeneradorIngreso{
    
    private double media;
    private double sigma;
    private Generador generador;
    
    public GeneradorIngreso(double media, double sigma,Generador generador) {
        this.media=media;
        this.sigma=sigma;
        this.generador=generador;
    }
        
    public double generarTiempoIngreso(){
        double varianza=sigma*sigma;
        double vary=Math.log((varianza/(media*media))+1);
        double medy=(Math.log(media)-(0.5))*Math.log((varianza/(media*media))+1);
        double sigy=Math.sqrt(vary);
        double sumy=0;
        for(int i=0;i<12;i++){
            sumy+=generador.generar();
        }
        return Math.exp(medy+(sigy*(sumy-6)));        
    }
    
}
