/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadores;

/**
 *
 * @author czarate
 */
public class AnalizadorAleatoriedad {
    
    private Object[] valores;
    
    public AnalizadorAleatoriedad(Object[] valores){
        this.valores=valores;
    }
    
    public int[] getFRelativas(){
        int[] serie=new int[10];
        for(int i=0;i<10;i++){
            double valor=(Double)valores[i];
            if(valor>=0&&valor<1){
                serie[0]++;
            }
            if(valor>=1&&valor<2){
                serie[1]++;
            }
            if(valor>=2&&valor<3){
                serie[2]++;
            }
            if(valor>=3&&valor<4){
                serie[3]++;
            }
            if(valor>=4&&valor<5){
                serie[4]++;
            }
            if(valor>=5&&valor<6){
                serie[5]++;
            }
            if(valor>=6&&valor<7){
                serie[6]++;
            }
            if(valor>=7&&valor<8){
                serie[7]++;
            }
            if(valor>=8&&valor<9){
                serie[8]++;
            }
            if(valor>=9&&valor<10){
                serie[9]++;
            }
        }
        return serie;
    }
    
    public int[] getFRelativasAcumuladas(){
        int[] series=new int[10];
        int[] acumuladas=getFRelativas();
        for(int i=0;i<10;i++){
            if(i==0){
                series[i]=acumuladas[i];
            }else{
                series[i]=series[i-1]+acumuladas[i];
            }
        }
        return series;
    }
    
    public double chiCalculada(){
        int[] prueba=getFRelativas();
        int VEi=valores.length/10;
        int E=0;
        for(int i=0;i<10;i++){
            int VOi=prueba[i];
            E+=VOi-VEi;
        }
        return (Math.pow(E,2))/VEi;
    }
    
}
