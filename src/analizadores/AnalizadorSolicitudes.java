/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadores;

import entidades.SolicitudEstadistica;
import herramientas.LectorCSV;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author czarate
 */
public class AnalizadorSolicitudes {
    
    private String url;
    private Object[] categorias;
    
    public AnalizadorSolicitudes(String url){
        this.url=url;
    }
    
    public List<SolicitudEstadistica> analizar(){
        List<SolicitudEstadistica> lista=new ArrayList();
        LectorCSV lector1=new LectorCSV(url);
        Object[] numerosEstadisticos=lector1.leerColumna(0);
        lector1.cerrar();
        LectorCSV lector2=new LectorCSV(url);
        Object[] numerosCategorias=lector2.leerColumna(1);
        lector2.cerrar();
        obtenerCategorias(numerosCategorias);
        double rango=0;
        for(int j=0;j<categorias.length;j++){
            List numeros=new ArrayList();
            for(int i=0;i<numerosEstadisticos.length;i++){
                if(numerosCategorias[i].equals(categorias[j])){
                    numeros.add(numerosEstadisticos[i]);
                }
            }
            SolicitudEstadistica se=new SolicitudEstadistica();
            se.setNombre(String.valueOf(categorias[j]));
            se.setRango_minimo(rango);
            rango+=(double)numeros.size()/numerosEstadisticos.length;
            se.setRango_maximo(rango);
            TendenciaCentral tc=new TendenciaCentral(numeros.toArray());
            double media=tc.obtenerMedia();
            se.setMedia(media);
            se.setMediana(tc.obtenerMediana());
            se.setModa(tc.obtenerModa());
            DispersionCentral dc=new DispersionCentral(numeros.toArray(),media);
            se.setVarianza(dc.obtenerVarianza());
            se.setDesviacion_estandar(dc.obtenerDesviacionEstandar());
            se.setCoeficiente_variacion(dc.obtenerCoeficienteVariacion());
            lista.add(se);
        }
        return lista;
    }
    
    private void obtenerCategorias(Object[] cat){
        List c=new ArrayList();
        for(int i=0;i<cat.length;i++){
            if(!c.contains(cat[i])){
                c.add(cat[i]);
            }
        }
        categorias=c.toArray();
        Arrays.sort(categorias);
    }
    
}
