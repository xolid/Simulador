/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadores;

import entidades.EntradaEstadistica;
import herramientas.LectorCSV;
import javax.swing.JTextArea;

/**
 *
 * @author czarate
 */
public class AnalizadorEntradas {
    
    private String url;
    
    public AnalizadorEntradas(String url){
        this.url=url;
    }
    
    public EntradaEstadistica analizar(){
        LectorCSV lector=new LectorCSV(url);
        Object[] estadisticos=lector.leerColumna(0);
        lector.cerrar();
        TendenciaCentral tc=new TendenciaCentral(estadisticos);
        EntradaEstadistica ee=new EntradaEstadistica();
        double media=tc.obtenerMedia();
        ee.setMedia(media);
        ee.setMediana(tc.obtenerMediana());
        ee.setModa(tc.obtenerModa());
        DispersionCentral dc=new DispersionCentral(estadisticos,media);
        ee.setVarianza(dc.obtenerVarianza());
        ee.setDesviacion_estandar(dc.obtenerDesviacionEstandar());
        ee.setCoeficiente_variacion(dc.obtenerCoeficienteVariacion());
        return ee;
    }
    
}
