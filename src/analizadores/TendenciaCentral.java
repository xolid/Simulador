/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadores;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author czarate
 */
public class TendenciaCentral {
    
    private double[] numeros;
    
    public TendenciaCentral(double[] numeros){
        this.numeros=numeros;
    }
    
    public TendenciaCentral(Object[] objetos){
        numeros=new double[objetos.length];
        for(int i=0;i<objetos.length;i++){
            numeros[i]=(double)objetos[i];
        }
    }
    
    public double obtenerMedia(){
        return obtenerSumatoria()/numeros.length;
    }
    
    public double obtenerMediana(){
        double[] n=numeros;
        Arrays.sort(n);
        if(n.length%2==0){
            return (n[(n.length)/2]+n[(n.length+1)/2])/2;
        }else{
            return n[(n.length+1)/2];
        }
    }
    
    public double obtenerModa(){
        int frecmax=0;
        double moda=0;
        List evaluados=new ArrayList();
        for(int i=0;i<numeros.length;i++){
            if(!evaluados.contains(numeros[i])){
                int frec=obtenerFrecuencia(numeros[i]);
                if(frec>frecmax){
                    frecmax=frec;
                    moda=numeros[i];
                }
                evaluados.add(numeros[i]);
            }
        }
        return moda;
    }
    
    private int obtenerFrecuencia(double numero){
        int frecuencia=0;
        for(int i=0;i<numeros.length;i++){
            if(numero==numeros[i]){
                frecuencia++;
            }
        }
        return frecuencia;
    }
    
    private double obtenerSumatoria(){
        double sumatoria=0;
        for(int i=0;i<numeros.length;i++){
            sumatoria+=numeros[i];
        }
        return sumatoria;
    }
    
}
