/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadores;

/**
 *
 * @author czarate
 */
public class DispersionCentral {
    
    private double[] numeros;
    private double media;
    
    public DispersionCentral(double[] numeros,double media){
        this.media=media;
        this.numeros=numeros;
    }
    
    public DispersionCentral(Object[] objetos,double media){
        this.media=media;
        numeros=new double[objetos.length];
        for(int i=0;i<objetos.length;i++){
            numeros[i]=(double)objetos[i];
        }
    }
    
    public double obtenerVarianza(){
        double suma=0;
        int total=numeros.length;
        for(int i=0;i<total;i++){
            suma+=Math.pow((numeros[i]-media),2);
        }
        return suma/total;
    }
    
    public double obtenerDesviacionEstandar(){
        return Math.sqrt(obtenerVarianza());
    }
    
    public double obtenerCoeficienteVariacion(){
        return obtenerDesviacionEstandar()/media;
    }
    
}


